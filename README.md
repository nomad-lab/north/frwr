# frwr


## Nomad configuration

```yaml
      frwr:
        description: FRWR3 in-line holography/focus series reconstruction code
        external_mounts: []
        file_extensions: []
        icon: logo/jupyter.svg
        image: gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-remote-tools-hub/frwr-webtop
        image_pull_policy: Always
        maintainer:
        - email: markus.kuehbach@physik.hu-berlin.de
          name: "Markus K\xFChbach"
        mount_path: /config
        privileged: true
        short_description: Inline electron holography by C. Koch
        with_path: false
```
